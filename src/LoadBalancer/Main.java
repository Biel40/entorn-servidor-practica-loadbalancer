package LoadBalancer;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception {

     Request request = new Request("https://www.esliceu.com/");

     Member member1 = new Member("gabrielborras1");
     Member member2 = new Member("javierviñas2");
     Member member3 = new Member("ivancaballero3");
     Member member4 = new Member("tonimesquida4");

     ArrayList<Member> membres = new ArrayList<>();
     membres.add(member1);
     membres.add(member2);
     membres.add(member3);
     membres.add(member4);

     LoadManager balancer = LoadManager.getInstance();
     balancer.setMembers(membres);

     String result = request.getUrl();
     result += balancer.handleRequest(request, membres,"Roundrobin", "CPU");

     System.out.println("La url de redirecció és aquesta : " + result);

    }

}
