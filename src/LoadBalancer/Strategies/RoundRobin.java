package LoadBalancer.Strategies;

import LoadBalancer.LoadManager;
import LoadBalancer.Member;

import java.io.IOException;
import java.util.ArrayList;

public class RoundRobin implements BalancingStrategy {

    public RoundRobin() {}

    @Override
    public Member nextMember(String probeType) throws IOException {

        LoadManager lm = LoadManager.getInstance();
        ArrayList<Member> members = lm.getMembers();

        for (Member member : members){
            if (member.isEnabled()){
                int load = member.getLoadValue(probeType);

                if (load < 80){
                    return member;
                }
            }
        }
        return null;
    }


}
