package LoadBalancer.Strategies;

import LoadBalancer.LoadManager;
import LoadBalancer.Member;
import LoadBalancer.Probes.Probe;

import java.io.IOException;
import java.util.ArrayList;

public class Random implements BalancingStrategy {

    @Override
    public Member nextMember(String probeType) throws IOException {
        LoadManager lm = LoadManager.getInstance();
        ArrayList<Member> members = lm.getMembers();

        Member member = members.get((int) (Math.random()* members.size()));

        if (member.isEnabled()){
            Probe probe = member.getProbe(probeType);
            // EL 80 hauria d'esser una constant!!!
            if (probe.inspectMember() < 80){
                return member;
            }
        }
        return member;
    }
}
