package LoadBalancer.Strategies;

import LoadBalancer.Member;

import java.io.IOException;

public interface BalancingStrategy {
    Member nextMember(String probeType) throws IOException;
}
