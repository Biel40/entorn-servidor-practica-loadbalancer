package LoadBalancer.Probes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProbeCPU implements Probe {
    @Override
    public int inspectMember() throws IOException {
        ProcessBuilder builder = new ProcessBuilder(
                "cmd.exe", "/c", "wmic cpu get loadpercentage");
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;

        while (true) {
            line = r.readLine();
            if (line == null) { break;}

            if (line.length() > 0){
                System.out.println(line);
            }
        }
        return 0;
    }
}
