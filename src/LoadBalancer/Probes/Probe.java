package LoadBalancer.Probes;

import java.io.IOException;

public interface Probe {
    int inspectMember() throws IOException;
}
