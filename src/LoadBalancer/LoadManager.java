package LoadBalancer;

import LoadBalancer.Strategies.Random;
import LoadBalancer.Strategies.RoundRobin;

import java.io.IOException;
import java.util.ArrayList;

public class LoadManager {

    private static LoadManager instance;

    private ArrayList<Member> members;

    private LoadManager(){

    }

    public static synchronized LoadManager getInstance(){

        if (instance == null){
            instance = new LoadManager();
            return instance;
        }else return instance;

    }
    
    public String getStrategyInstance(String strategy){
        if (strategy.equals("Roundrobin")) return new RoundRobin();
        if (strategy.equals("random")) return new Random();
        return null;
        
    }

    public String handleRequest(Request request, ArrayList<Member> members, String strategy, String type) throws IOException {
        //** XAXO-01 - AQUI HI HA UN MAL US DEL POLIMORFISME- SEMBLA QUE NO ENTENS EL POLIMORFISME*/
        
        BalancingStrategy balancingStrategy = getStrategyInstance(strategy);
        Member member = roundRobin.nextMember(type);
        
                   
            if (member == null){
                return "There is no member available!";
            }
            
            return member.getUrl();
        
       
    }

    public ArrayList<Member> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<Member> members) {
        this.members = members;
    }
}
