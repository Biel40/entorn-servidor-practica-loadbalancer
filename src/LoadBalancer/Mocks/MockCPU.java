package LoadBalancer.Mocks;
import LoadBalancer.Probes.Probe;

import java.util.Random;

public class MockCPU implements Probe {

    @Override
    public int inspectMember() {
        Random random = new Random();
        int value = random.nextInt(100);
        System.out.println("MockCPU Load : " + value);
        return value;
    }


}
