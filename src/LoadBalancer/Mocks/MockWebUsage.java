package LoadBalancer.Mocks;

import LoadBalancer.Probes.Probe;
import java.io.IOException;
import java.util.Random;

public class MockWebUsage implements Probe {

    @Override
    public int inspectMember() throws IOException {
        Random random = new Random();
        int value = random.nextInt(100);
        System.out.println("MockWebLoad : " + value);
        return value;
    }
}
