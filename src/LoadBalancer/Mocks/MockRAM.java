package LoadBalancer.Mocks;

import LoadBalancer.Probes.Probe;

import java.util.Random;

public class MockRAM implements Probe {
    @Override
    public int inspectMember() {
        Random random = new Random();
        int value = random.nextInt(100);
        System.out.println("MockRAM Load : " + value);
        return value;
    }
}
