package LoadBalancer;

import LoadBalancer.Mocks.MockCPU;
import LoadBalancer.Mocks.MockDisk;
import LoadBalancer.Mocks.MockRAM;
import LoadBalancer.Mocks.MockWebUsage;
import LoadBalancer.Probes.*;

import java.io.IOException;

public class Member {

    private String url;

    Member(String url){
        this.url = url;
    }

    public boolean isEnabled(){
        return true;
    }

    public String getUrl() {
        return url;
    }

    public String toString(){
        return this.url;
    }

    public Probe getProbe(String probeType){

        switch (probeType){
            // Li pas un Mock , ja que no tenc implementacions reals.
            case "CPU": return new MockCPU();

            case "DISK": return new MockDisk();

            case "RAM": return new MockRAM();

            case "WEB": return new MockWebUsage();

            default: return null;
        }
    }

    public int getLoadValue(String type) throws IOException {
        Probe probe = getProbe(type);
        return probe.inspectMember();
    }

}
